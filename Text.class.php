<?php 
class Text {
    private $text ;
    private $explode;

    public function __construct($file) {
        define( 'CHAR_TRUE', '=' ) ;
        define( 'CHAR_FALSE', '~' );
        $this->text = file_get_contents($file);
        $this->explode = explode('//', $this->text) ;
    }

    // metodo para optener las 3 primeras filas que son reconocidas como titulos 
    // argumento para configurar el numero de lineas a reconocer como cabecera 
    public function getTile($fileTiles){

        $tiles = '';
        for ($i = 0 ; $i <= $fileTiles ; $i++) {
            $tiles .= $this->explode[$i] . '<br>';
        }

        return $tiles ;
    }
    
    // metodo que divide el string entre pregunta y respuestas 
    private function getQuestions($fileTiles){
        $count = count($this->explode);
        $explode_questions = array();

        for ($i = $fileTiles + 1 ; $i < $count ; $i++) {
            $questions[$i] = $this->explode[$i];

            $explode_questions[$i] = explode('{' , $questions[$i]) ;
        }

        return  $explode_questions ;
    }

    // metodo que identifica y devuelve la pregunta 
    public function getQuest($fileTiles) {
        
        $quest = array();
        $explode_questions = $this->getQuestions($fileTiles) ;

        foreach($explode_questions as $key => $val){
            $quest[$key] = $val[0] ;
        }

        return $quest ;
    }
    
    // metodo que devuelve todas las respuestas en un array y nos indica su valor 
    public function getAnswer($fileTiles) {
        $answer = array();
        $explode_questions = $this->getQuestions($fileTiles) ;
        foreach($explode_questions as $key => $val){
        if(!array_key_exists(1 , $val)) $val[1] = '' ; 

            $str_answer = str_replace('}', '' , $val[1]);

            $i = 0 ;
            $lastPos = 0 ;
            
            while ($pos[$key][$i] = strpos($str_answer , '~' , $lastPos)){

                $lastPos = $pos[$key][$i]  + 1  ;
                $i++;
                
            }

            $pos = $pos[$key] ;

            $pos[$i] = strpos($str_answer , '=' , 0 ) ; 
            sort($pos) ;
            
            for($n = 0 ; $n <= $i ; $n++ ){

                $lenght =  (array_key_exists($n+1 , $pos)) ? $pos[ $n+1 ] - $pos[$n] :  $pos[$n] - count($pos[$n]) ;

                $answer[$key][$n] = substr($str_answer , $pos[$n] , $lenght );               

            }

        }

        return  $answer ;
    }
}