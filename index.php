<?php
// Desde aqui se elige el test que se desea rellenar.
// Los test son guardados en la carpeta tests con extension .txt
// Para ser reconocidos tiene un formato especifico.
// mas información README.md del proyecto  

session_start();

// scandir para leer todos los archivos que tiene la carpeta tests 

$files = scandir('tests');

// acomodo el array
unset($files[0] , $files[1]);

?>
<!doctype html>
<html lang="es" >
    <head> 
        <meta charset="UTF-8">
            <link rel=stylesheet type="text/css" href='style.css'>
        <title></title>
    </head>
    <body>
        <form action = 'test.php' method='GET'>
            <select name="test">
                <?php
                    //bucle de lectura que añade las opciones en el select inicial, 
                    // una opcion por cada test que exista en la carpeta tests
                    foreach ($files as $file) {
                        ?>
                        <option value="<?= $file?>"><?= $file?></option>
                        <?php
                    }
                ?>
            </select>
            <p></p>
            <button>Aceptar</button>
        </form>

    </body>
</html>