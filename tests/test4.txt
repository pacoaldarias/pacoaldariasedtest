﻿// ED
// TEMA 4. ED. DIAG. ACTIV.
// Francisco Aldarias.
//1
Un diagrama de actividad es un diagrama uml de:
{
~Diagrama de acción
=Diagrama de comportamiento
~Diagrama de ordenación
~Ninguno de los citados.
}


//2
El actor en un caso de uso hace referencia en un diagrama de actividad a:
{
~Objeto
=Calle
~Actividad
~Ninguno de los citados.
}


//3
Varios extend en un caso de uso hace referencia en un diagrama de actividad a:
{
~Varias flechas que llegan a la misma actividad.
= Varias salidas de un rombo
~ Varias actividades unidas por flechas
~Ninguno de los citados.
}


//4
El diagrama de actividad  para indicar que se pueda hacer varias  actividades  a la vez, usaremos … 
{
~Mediante flechas especiales
= Líneas de sincronización
~ Mediante línea continuas.
~Ninguno de los citados.
}


// 5
En un  diagrama de actividad los objetos se dibujan como...
{
~ cuadrados
~ cuadrados redondeados
~ rectángulos redondeados
=Ninguno de los citados.
}


// 6
En un  diagrama de actividad para representan resultados o valores de entrada a alguna actividad se utilizan ...
{
~ cuadrados
= objetos
~ clases
~ flechas
}


// 7
En un  diagrama de actividad como se representa el inicio del ﬂujo o actividad inicial … 
{
~ cuadrado negro
= círculo negro
~ círculo negro rodeado de un circunferencia
~ cuadrado negro rodeado de un circunferencia
}


// 8
En un  diagrama de actividad como se representa el final del ﬂujo o actividad final … 
{
~ cuadrado negro
~ círculo negro
= círculo negro rodeado de un circunferencia
~ cuadrado negro rodeado de un circunferencia
}


// 9
En un diagrama de actividad cuando un rombo es decisión 
{
~ Salen dos o más flujos sin condiciones
~ Sale un flujo con condición
= Salen dos o más flujos con condiciones
~  Sale un flujo sin condición
}


// 10
En un diagrama de actividad  tiene sentido tener una sola entrada a una linea de concurrencia?
{
= No
~ Si
}


// 11
En un diagrama de actividad  tiene sentido tener un rombo de decisión con las flechas hacia una línea de concurrencia?
{
= No
~ Si
}


// 12
En un diagrama de actividad las calles corresponden con los actores del diagrama de casos de uso?
{
~No
=Si
}


// 13
En un diagrama de actividad existen objetos de un clase?
{
~No
=Si
}


// 14
Es cierto que en un diagrama de actividad todas las líneas que entran a un rombo de decisión se deben etiquetas con condiciones?
{
=No
~Si
}


// 15
Es cierto que en un diagrama de actividad todas las líneas que salen del rombo de decisión se deben etiquetas con condiciones?
{
~No
=Si
}


// 16
Es cierto que en un diagrama de actividad puede haber más de un inicio?
{
=No
~Si
}


// 17
Es cierto que en un diagrama de actividad puede haber más de un final?
{
=Si
~No
}


// 18
En un diagrama de actividad cuando un rombo es unión. Cuál es la incorrecta ... 
{
~ Llegan muchas lineas
~ Sale sólo una linea
= Salen muchas lineas
~ Llega una linea
}
