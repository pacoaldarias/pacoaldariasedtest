﻿// ED. TEMA 5. TEST AUTOEVALUABLE.
//
// PACO ALDARIAS

//1
Para poder depurar un programa en java debemos antes computarlo con ...
{
~javac Miclase.java
=javac -g Miclase.java
~javac Miclase
~javac -g Miclase
}


//2
Para poder entrar al depurador debemos poner el comando ...
{
~debug Miclase.java
=jdb Miclase
~jdb Miclase.java
~jdb -g Miclase
}


//3
Para poder empezar inicialmente a depurar un programa debemos poner … 
{
~init
=run
~start
~cont
}


//4
Que comando no realiza la depuración hasta que encuentra un punto de parada o termina … 
{
~cont
=start
~run
~todos bien
}


// 5
Que comando permite ver los brekpoints … 
{
~ breakpoint
~ list
~ show
= clear
}


// 6
Que comando  crea un breakpoint en el inicio de una función llamada mifuncion en la clase miclase … 
{
~stop at Miclase.mifuncion
~stop Miclase.mifuncion
=stop in Miclase.mifuncion
~stop in Miclase:mifuncion
}




// 7
Que comando crea un breakpoint en la un linea en la clase Miclase … 
{
~stop at Miclase.linea
~stop Miclase:linea
~stop in Miclase:linea
=stop at Miclase:linea
}


// 8
Que comando  borra un breakpoint en el inicio de una función llamada mifuncion en la clase miclase … 
{
~clear Miclase:mifuncion
~clear at Miclase.mifuncion
=clear Miclase.mifuncion
~clear in Miclase:mifuncion
}




// 9
Que comando borra un breakpoint en la un linea en la clase Miclase … 
{
~clear at Miclase.linea
~clear Miclase.linea
~clear in Miclase:linea
=clear Miclase:linea
}




// 10
Qué comando muestra las linea del codigo antes de hacer run en el depurador ...
{
~list
=todos son falsas.
~print
~show
}


// 11
Que comando permite cambiar el valor de una variable a y sumarle 1… 
{
= set a=a+1
~ a=a+1
~ let a=a+1
~ a++
}


// 12
Que comando permite avanzar el depurador una linea y si encuentra una función entre dentro de ella … 
{
= step
~ next
~ cont
~ run
}




// 13
Que comando permite avanzar el depurador una linea y si encuentra una función no entre dentro de ella … 
{
~ step
= next
~ cont
~ run
}


// 14
Cuando se llama a un constructor de una clase con el  comando step el depurador se va a la línea ...
{
~ todas mal
~ no avanza
~ siguiente
= 1
}


// 15
Con el comando list se puede ver la línea de código por la que va el depurador con el comando list ...
{
~aparece un símbolo mayor
=aparece marcada la linea con los símbolos igual y mayor
~aparece un asterisco
~aparece un $
}


// 16
Qué ocurre si ponemos un breakpoint en una linea que no hay código
{
=que no deja ponerlo
~no pasa nada
~pone el breakpoint en la línea anterior
~pone el breakpoint en la línea siguiente
}


// 17
El comando print equivale al comando ...
{
=eval
~show
~echo
~printf
}


// 18
Qué comando permite ejecutar línea a línea entrando en funciones...
{
=step
~stepi
~step up
~next 
}


// 19
Qué comando permite ejecutar instrucción a instrucción entrando en funciones es : 
~step
=stepi
~step up
~next 
}


// 20
Qué comando permite ejecutar hasta el final de la función entrando en funciones  es : 
~step
~stepi
=step up
~next 
}


// 21
Qué ocurre si no existe la clase que ponemos en el depurador: 
~da error al entrar
~no arranca
=no funciona run 
~están todas mal
}


// 22
Si compilamos con javac Miclase.java y lanzamos el depurador con jdb Miclase entonces:
~arranca el compilador pero no funciona run
~no arranca el compilador
=al ejecutar print en una variable devuelve null
~al ejecutar print en una variable devuelve el valor real.
}
