﻿// CUESTIONARIO AUTOEVALUABLE
// TEMA 3. CASOS DE USO.
// ED
//1
La especialización de actores se representa mediante:
{
~Una linea
=Una flecha
~Una doble flecha
~No es posible
}


//2
La especialización de actores la punta de la flecha apunta a:
{
~Al caso especilizado
=El caso general
~Es una linea
~Es una doble flecha
}


//3
Los casos de uso debe expresarse mediante:
{
~sustantivos
=verbos
~conjunciones
~artículos
}


//4
Marca la frase incorrecta:
{
~los casos de uso se relaciona mediante extend
~los casos de uso se relacionan con otros casos de uso mediante asociaciones (línea continua)
~los casos de uso se relaciona mediante include
=los casos de uso no se relacionan
}


//5
La relación extend indica que:
{
=es un caso de uso que puede o no hacer.
~es un caso de uso que siempre se hace
~es un caso de uso que no se hace nunca
~es un caso de uso que se hará dependiendo del actor
}






//7
La relación include indica que:
{
~es un caso de uso que puede o no hacer.
=es un caso de uso que siempre se hace
~es un caso de uso que no se hace nunca
~es un caso de uso que se hará dependiendo del actor
}


//8
Los casos de usos son diagramas que se aplican en la fase de
{
~diseño
=análisis de requisitos
~codificación
~pruebas
}


//9
Los casos de usos son diagramas son diagramas de :
{
=comportamiento
~estructura
~interacción
~ninguno de los anteriores
}


//10
Cual es el objetivo del caso de uso:
{
~Todas de las anteriores.
~Descripción de todas las acciones que se realizan en el sistema
~Poner todas las operaciones que se realizan
=Hacerte entender que hace la aplicación
}


//11
Las personas o cosas que interactúa con el sistema se llama en un caso de uso:
{
= actor
~ usuario
~ sistema
~ ninguna de las que hay
}




//12
Cuantos diagramas de casos de uso haremos:
{
~Uno
=Los necesarios para que sea legible.
~Tantos como actores
~Uno por departamento.
}


//13
Se le llama actor a toda 
{
~entidad externa o interna al sistema
~entidad interna al sistema
=entidad externa al sistema
~todas son ciertas
}